import axios from "axios";

const addProduct = async () => {
  const requestData = {
    title: "New Shoe product",
    price: 1222,
    description: "New nike shoe",
    categoryId: 1,
    images: [
      "https://fastly.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U",
    ],
  };
  const response = await axios({
    method: "post",
    data: requestData,
    url: "https://api.escuelajs.co/api/v1/products",
  });

  return response.data;
};

export default addProduct;
