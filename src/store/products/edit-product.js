import axios from "axios";

const editProduct = async () => {
  const requestData = {
    title: "New Shoe 2",
    price: 12000,
    description: "New nike shoe 1",
    categoryId: 1,
    images: [
      "https://fastly.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U",
    ],
  };
  const response = await axios({
    method: "put",
    data: requestData,
    url: "https://api.escuelajs.co/api/v1/products/232",
  });

  return response.data;
};

export default editProduct;
