import axios from "axios";

const getProductList = async () => {
  const response = await axios({
    method: "get",
    url: "https://api.escuelajs.co/api/v1/products/",
  });

  return response.data;
};

export default getProductList;
