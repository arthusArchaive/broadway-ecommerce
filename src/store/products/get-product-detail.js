import axios from "axios";

const getProductDetail = async () => {
  const response = await axios({
    method: "get",
    url: "https://api.escuelajs.co/api/v1/products/232",
  });

  return response.data;
};

export default getProductDetail;
