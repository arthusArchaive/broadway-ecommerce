import { useEffect } from "react";
import getProductList from "./store/products/get-product-list";
import addProduct from "./store/products/add-product";
import getProductDetail from "./store/products/get-product-detail";
import editProduct from "./store/products/edit-product";

function App() {
  const fetchData = async () => {
    const response = await getProductList();
    console.log(response, "mee");
  };
  const fetchDetailData = async () => {
    const response = await getProductDetail();
    console.log(response, "mee detail");
  };
  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    fetchDetailData();
  }, []);

  return (
    <div>
      <button
        onClick={addProduct}
        className="bg-blue-100 p-4 rounded font-semibold"
      >
        Add Product
      </button>

      <button
        onClick={editProduct}
        className="bg-green-100 p-4 rounded font-semibold"
      >
        Edit Product
      </button>
    </div>
  );
}

export default App;
